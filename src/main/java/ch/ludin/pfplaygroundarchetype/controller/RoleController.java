/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.ludin.pfplaygroundarchetype.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import ch.ludin.pfplaygroundarchetype.entity.Role;
import ch.ludin.pfplaygroundarchetype.entity.User;
import ch.ludin.pfplaygroundarchetype.entity.UserRole;
import ch.ludin.pfplaygroundarchetype.persistence.DataRepository;



@Named("roleController")
@SessionScoped
public class RoleController extends AbstractController<Role> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	@DataRepository
	private EntityManager em;


	public RoleController(){
		super(Role.class);
	}
	
	public void deleteRole() {
		EntityTransaction tx = em.getTransaction();
		@SuppressWarnings("unchecked")
		List<UserRole> lustUserRole = em
				.createQuery("FROM UserRole ur WHERE ur.role.id =:idRole")
				.setParameter("idRole", getSelectedRecord().getId()).getResultList();
		for (UserRole ur : lustUserRole) {
			tx.begin();
			em.remove(ur);
			tx.commit();
		}
		tx.begin();
		em.remove(getSelectedRecord());
		tx.commit();

	}

	
	//@TestAnnotation
	public void test() {
		printFacesMessage("Hello", "this is a test by Dani");
	}
}