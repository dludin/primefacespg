A very primitive template for a JSF/Primefaces Playground with some JPA.
To get one up und running quickly for experimenting.

Start with mvn clean install tomcat7:run.
Watch the console output for the context name of the web app
and append this name to this url:

http://localhost:8080/

